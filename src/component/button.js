import React,{Component} from 'react'
import './button.css'
import {Button} from 'reactstrap'
const CustomButton = (props) => {
	return (
		<Button className='ProfileButtonCustomize' onClick={props.onClick} data-testid="button">
			<span className='ProfileButtonText'>{props.title}</span>
		</Button>
	)
}
export default CustomButton;