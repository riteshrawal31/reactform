import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import Form from '../pages/form'
import Table from '../pages/table'
const RouterComponent= () => {
	return (
		<Router>
			<Switch>
				<Route exact path='/' component={Form}></Route>
				<Route exact path='/table' component={Table}></Route>
				</Switch>
		</Router>
	)
}
export default RouterComponent