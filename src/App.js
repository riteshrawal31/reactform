import logo from './logo.svg';
import './App.css';
import RouterComponent from './router/router'
import FormPageComponent from './pages/form'
function App() {
  return (
    <div className="App">
     <RouterComponent/>
    </div>
  );
}

export default App;
