import React from 'react'
import {render,cleanup,fireEvent} from '@testing-library/react'
import Button from '../component/button'
 afterEach(cleanup);

it("render component",()=>{
    const {getByTestId}=render(<Button/> );
    const button=getByTestId('button');
    expect(button).toBeDefined();
})
it("ender button correctly",()=>{
    const {getByTestId}=render(<Button title="Save Changes" />)
    expect(getByTestId("button")).toHaveTextContent('Save Changes')
})
it("captures clicks", done => {
    function onNextdata() {
      done();
    }
    const { getByTestId } = render(
      <Button onClick={onNextdata}  title="Show Table"/>
    );
    const node = getByTestId("button");
    fireEvent.click(node);
  });