import React,{useEffect,useState} from 'react';
import { Table,Input } from 'reactstrap';
import './table.css'
const NewData=localStorage.getItem("data")
var TableData = JSON.parse(NewData);

const TableDisplay = (props) => {
const [newdata, setNewData] = useState([])

    const renderTableHeader=()=> {
        let header = Object.keys(TableData[0])
        return header.map((key, index) => {
           return <th key={index}>{key.toUpperCase()}</th>
        })
     }
     const renderTableData=()=> {
         const Dynamicdata=newdata.length===0?TableData:newdata
        return Dynamicdata.map((items, index) => {
           const { country, name, favorite_phone_brand, phone_number } = items //destructuring
           return (
              <tr key={index} >
                 <td>{name}</td>
                 <td>{country}</td>
                 <td>{favorite_phone_brand}</td>
                 <td>{phone_number}</td>
              </tr>
           )
        })
     }
    const  filterDataList = (event) => {
        console.log("event",event.target.value)
        console.log("event",newdata)
        var updatedList = TableData;
        updatedList = updatedList.filter(function(list) {
          return (
            list.name.toLowerCase().search(event.target.value.toLowerCase()) !==
            -1 || list.favorite_phone_brand.toLowerCase().search(event.target.value.toLowerCase()) !==
            -1
          );
        });
        setNewData(
           updatedList
    );
        };
  return (
      <div >
          <Input
          className="InputCustomize"
          placeholder="Name OR Favorite Phone Brand "
          name="newdata"
          onChange={filterDataList}
          />
           <Table bordered>
       
       
        <tbody>
        <tr>{renderTableHeader()}</tr>
          {renderTableData()}  
        </tbody>
     
       
             </Table>
         <div>
         </div>
       
       </div> 
  
  );
}

export default TableDisplay;