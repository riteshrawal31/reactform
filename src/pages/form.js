//import Library and third party
import React, { useState} from 'react'
import  {useHistory} from 'react-router-dom'
import { AvForm, AvField } from 'availity-reactstrap-validation';
import {FormGroup,Label} from 'reactstrap'
import CustomButton from '../component/button'
//import  Action and component
import './form.css'
const FormPageComponent = (
	props
) => {
    const history=useHistory()
	const [updateForm, setUpdateForm] = useState([
		{
            
			name: '',
			country: '',
			favorite_phone_brand: '',
			phone_number: '',
      
			
			
		},    
	])
	

	const handleAddFields = () => {
		let currentForm = updateForm

		if (currentForm.length ) {
			currentForm = [
				...currentForm,
				{
					name: '',
			        country: '',
			        favorite_phone_brand: '',
			        phone_number: '',
      
					
					
				
			  },
			]
		}
		setUpdateForm(currentForm);
	}
	const onFormDataChange = (event, formIndex) => {
		
		const { name, value } = event.target
		let newUpdateForm = updateForm.map((f, index) => {
			if (formIndex === index) {
                console.log('Hello value',value);
				return { ...f, [name]: value };
			} else {
				return f
			}
		})

		setUpdateForm(newUpdateForm)
	}
    const onSubmit=()=>{
        localStorage.setItem('data', JSON.stringify(updateForm));
        window.location.reload(false);
      
    }
    const onNextdata=()=>{
         props.history.push('/table')
    }


	return (
		<div className='Container' data-testid="form">
			<div className='FormContentContainer'>
				
				{updateForm.map((f, index) => (
					<div key={index}>
                         <AvForm onValidSubmit={onSubmit} onInvalidSubmit={onSubmit}>
                         <div className='FormInputContainer'>
                         <FormGroup className='FormInputDropDownField'>
                            <Label for='exampleSelect' className='SelectTextCustomize'>
                                Name
								</Label>
								<AvField
									type='text'
									name='name'
									className='FormInputDropTextField'
									placeholder='Name'
                                    validate={{
                                        required: {value: true, errorMessage: 'Please Enter Your Name'},
                                        pattern: {value: '^[A-Za-z0-9]+$', errorMessage: 'Your name must be composed only with letter and numbers'},
                                      }}
									onChange={(event) => onFormDataChange(event, index)}
									id='exampleSelect'
								></AvField>
							</FormGroup>
                            </div>
                            <div className='FormInputContainer'>
							<FormGroup className='FormInputDropDownField'>
								<Label for='exampleSelect' className='SelectTextCustomize'>
                                Country
								</Label>
								<AvField
									type='text'
									name='country'
									className='FormInputDropTextField'
									placeholder='Country'
                                    validate={{
                                        required: {value: true, errorMessage: 'Please Enter Country Name'},
                                        pattern: {value: '^[A-Za-z]+$', errorMessage: 'Your name must be composed only with letter'},

                                      }}
									onChange={(event) => onFormDataChange(event, index)}
									id='country'
								></AvField>
							</FormGroup>
							
						</div>
                        <div className='FormInputContainer'>
							<FormGroup className='FormInputDropDownField'>
								<Label for='exampleSelect' className='SelectTextCustomize'>
								Favorite Phone Brand
								</Label>
								<AvField
									type='text'
									name='favorite_phone_brand'
									className='FormInputDropTextField'
									placeholder='Favorite Phone Brand'
                                    validate={{
                                        required: {value: true, errorMessage: 'Please Enter Favorite Phone Brand'},
                                      
                                      }}
									onChange={(event) => onFormDataChange(event, index)}
									id='phonebrand'
								></AvField>
							</FormGroup>
							
						</div>
                        <div className='FormInputContainer'>
							<FormGroup className='FormInputDropDownField'>
								<Label for='exampleSelect' className='SelectTextCustomize'>
                                Phone Number
								</Label>
								<AvField
									type="number"
									name='phone_number'
									className='FormInputDropTextField'
									placeholder='Phone Number'
                                    validate={{
                                        required: {value: true, errorMessage: 'Please Enter Phone Number'},
                                        maxLength: {value: 10, errorMessage: 'Your Phone Number must be 10 characters'},
                                        
                                      }}
									onChange={(event) => onFormDataChange(event, index)}
									id='phonenumber'
								></AvField>
							</FormGroup>
							
						</div>
                    
                      </AvForm>
						
					</div>
				))}
				<div className='ButtonContainer'>
					<CustomButton  onClick={handleAddFields} title="Add New Form"/>
				</div>
                <div className='ButtonContainer'>
					<CustomButton  onClick={onSubmit} title="Save Changes" />
				</div>
                <div className='ButtonContainer'>
					<CustomButton  onClick={onNextdata} title="Show Table" />
				</div>
			</div>
		</div>
	)
}

export default FormPageComponent
