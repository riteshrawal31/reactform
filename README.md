# `Dynamic-Form`

##### This is a Simple Form Assignmaent  developed using React.js.
-------------------------------------------------------------------------------
### Getting Started

### Installing

A step by step  guide that tell you how to get a development env running

**_`Clone the gitlab project`_**
```sh
 $ https://gitlab.com/riteshrawal31/reactform.git
```
**_`Navigate to project directory`_**
```sh
 $ cd reactform
```

**_`Install the dependencies`_**
```sh
 $ yarn install 
```

**_`Start the Application on web-server`_**
```sh
 $ yarn start 
```
 #### **Simple build for production** ####

```sh
 $ yarn build 
```
 #### **Run the Tests** ####

```sh
 $ yarn test
```
